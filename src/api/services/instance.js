import axios from "axios";

export const searchApi = "/search"

export const api = axios.create({
    baseURL: "https://your_base_api"
});

export default api
