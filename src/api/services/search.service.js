import { api, adhocSearchApi } from "./instance";

export const adhocSearch = async ( options, criteria ) => {

    const params = {
        options,
        criteria
    }
    console.log(criteria)
    console.log(options)

    const results = await api.get(adhocSearchApi, params);
    if (results) {
        return results.data ? results.data : results;
    }
}
