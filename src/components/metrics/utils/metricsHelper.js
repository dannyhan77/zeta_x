const metricsHelper = {

    generateReportOneBarData(rawData) {
        console.log(rawData);

        const barData = {
            mappings: {
                "europe": "Europe",
                "namerica": "North America",
                "asia": "Asia",
                "lamerica": "Latin",
                "meast": "Middle East",
                "africa": "Africa"
            }
            ,
            chartData: [{
                "month": "2022-01",
                "europe": 2.5,
                "namerica": 2.5,
                "asia": 2.1,
                "lamerica": 1,
                "meast": 0.8,
                "africa": 0.4
            }, {
                "month": "2022-02",
                "europe": 2.6,
                "namerica": 2.7,
                "asia": 2.2,
                "lamerica": 0.5,
                "meast": 0.4,
                "africa": 0.3
            }, {
                "month": "2022-03",
                "europe": 2.8,
                "namerica": 2.9,
                "asia": 2.4,
                "lamerica": 0.3,
                "meast": 0.9,
                "africa": 0.5
            }]
        };

        return barData;
    },

    generateReportTwoBarData(rawData) {
        console.log(rawData);

        const barData = {
            mappings: {
                "europe": "Europe",
                "namerica": "North America",
                "asia": "Asia",
                "lamerica": "Latin",
                "meast": "Middle East",
                "africa": "Africa"
            }
            ,
            chartData: [{
                "month": "2022-01",
                "europe": 2.5,
                "namerica": 2.5,
                "asia": 2.1,
                "lamerica": 1,
                "meast": 0.8,
                "africa": 0.4
            }, {
                "month": "2022-02",
                "europe": 2.6,
                "namerica": 2.7,
                "asia": 2.2,
                "lamerica": 0.5,
                "meast": 0.4,
                "africa": 0.3
            }]
        };
        return barData;
    },

    generateReportOnePieData(rawData) {
        console.log(rawData);
        const data = [
            {value: 10, category: "One"},
            {value: 9, category: "Two"},
            {value: 6, category: "Three"},
            {value: 5, category: "Four"},
            {value: 4, category: "Five"},
            {value: 3, category: "Six"},
            {value: 1, category: "Seven"},
        ];
        return data;
    },

    generateReportTwoPieData(rawData) {
        console.log(rawData);
        const data = [
            {value: 10, category: "USA"},
            {value: 9, category: "CHINA"},
            {value: 6, category: "CANADA"},
            {value: 5, category: "MEXICO"},
        ];
        return data;
    }
}

export { metricsHelper }
