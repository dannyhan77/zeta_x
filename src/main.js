import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import PrimeVue from "primevue/config";
import { ObserveVisibility } from 'vue-observe-visibility';

// font awesome stuff
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { dom } from '@fortawesome/fontawesome-svg-core'

import './assets/scss/app.scss'

import 'typeface-roboto/index.css'
import 'typeface-poppins/index.css'

library.add(fas)
dom.watch() // replaces i to svg tags and configures a MutationObserver

const app = createApp(App).use(store).use(router).use(PrimeVue)

// font awesome stuff
app.component('font-awesome-icon', FontAwesomeIcon)
// end of font awesome stuff

app.directive('observe-visibility', {
    beforeMount: (el, binding, vnode) => {
        vnode.context = binding.instance;
        ObserveVisibility.bind(el, binding, vnode);
    },
    update: ObserveVisibility.update,
    unmounted: ObserveVisibility.unbind,
});

app.mount('#app')
