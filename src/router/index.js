import { createWebHistory, createRouter } from "vue-router";

import Layout from "../layouts/Layout";

const routes = [
    {
        path: "/",
        name: "home",
        component: Layout,
        children: [
            {
                path: "/",
                name: "sampleView",
                component: () => import(/* webpackChunkName: "about" */ '../components/sample/Sample')
            },
            {
                path: "/autoSuggest",
                name: "autoSuggest",
                component: () => import(/* webpackChunkName: "about" */ '../components/sample/AutoSuggest')
            },
            {
                path: "/dropdowns",
                name: "dropdowns",
                component: () => import(/* webpackChunkName: "about" */ '../components/sample/page/DropdownSamples')
            },
            {
                path: "/tabs",
                name: "tabs",
                component: () => import(/* webpackChunkName: "about" */ '../components/sample/page/TabsSample')
            },
            {
                path: "/fullWidthWithSidebar",
                name: "fullWidthWithSidebar",
                component: () => import(/* webpackChunkName: "about" */ '../components/sample/page/FullWidthWithSidebar')
            },
            {
                path: "/siteMetrics",
                name: "siteMetrics",
                component: () => import(/* webpackChunkName: "about" */ '../components/metrics/SiteMetricsView')
            }
        ]
    },

];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;
