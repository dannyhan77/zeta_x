import { createStore } from 'vuex'
import VuexPersist from 'vuex-persist'
import _ from 'lodash'

import app from './modules/app.module'

const vuexLocalStorage = new VuexPersist({
    key: 'vuex', // The key to store the state on in the storage provider.
    storage: window.localStorage, // or window.sessionStorage or localForage
    // Function that passes the state and returns the state with only the objects you want to store.
    // reducer: state => ({user: state.user}),
    modules: ['app']
    // Function that passes a mutation and lets you decide if it should update the state in localStorage.
    // filter: mutation => (true)
})

export const storeModules = {
    app
}

const store = createStore({
    modules: _.cloneDeep(storeModules),
    plugins: [vuexLocalStorage.plugin],
    mutations: {
        resetState(state) {
            _.forOwn(storeModules, (value, key) => {
                state[key] = _.cloneDeep(value.state)
            })
        }
    }
})

export default store
