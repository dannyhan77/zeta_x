import * as AppTypes from './../types/app.types'

const module = {
    namespaced: true,
    state: {
        theme: ''
    },
    getters: {},
    mutations: {
        [AppTypes.CURRENT_THEME](state, payload) {
            state.theme = payload
        }
    },
    actions: {
        updateCurrentTheme({ commit }, payload) {
            commit(AppTypes.CURRENT_THEME, payload)
        }
    }
}

export default module
