module.exports = {
  purge: {
    content: [
      "./index.html",
      "./src/**/*.{vue,js,ts,jsx,tsx}",
    ],
    options: {
      safelist: [/svg.*/, /fa.*/]
    }
  },
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    extend: {
      width: {
        '170': '42.5rem',
        '200': '50rem'
      },
      maxWidth: {
        '8xl': '88rem',
        '9xl': '96rem'
      },
      minHeight: {
        '1/2': '50%',
        '16': '4rem',
        '20': '5rem',
        '32': '8rem'
      },
      fontFamily: {
        roboto: ['Roboto']
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    // ...
    require('@tailwindcss/forms'),
  ],
}
